FROM node

RUN mkdir /skillbox
WORKDIR /skillbox

COPY ./flatris/package.json .
COPY ./flatris/yarn.lock .
RUN yarn install

ADD ./flatris /skillbox

RUN yarn install
RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
